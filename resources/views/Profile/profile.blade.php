@extends('layouts.common')
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<style>
    .card {
        width: 100%;
    }

    .avatar-upload {
        position: relative;
        max-width: 205px;
        margin: 50px auto;
    }

    .avatar-upload .avatar-edit {
        position: absolute;
        right: 12px;
        z-index: 1;
        top: 10px;
    }

    .avatar-upload .avatar-edit input {
        display: none;
    }

    .avatar-upload .avatar-edit input+label {
        display: inline-block;
        width: 34px;
        height: 34px;
        margin-bottom: 0;
        border-radius: 100%;
        background: #FFFFFF;
        border: 1px solid transparent;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
        cursor: pointer;
        font-weight: normal;
        transition: all 0.2s ease-in-out;
    }

    .avatar-upload .avatar-edit input+label:hover {
        background: #f1f1f1;
        border-color: #d6d6d6;
    }

    .avatar-upload .avatar-edit input+label:after {
        content: "\f044" !important;
        font-family: 'Font Awesome 5 Free';
        color: #757575;
        position: absolute;
        top: 10px;
        left: 0;
        right: 0;
        text-align: center;
        margin: auto;
    }

    .avatar-upload .avatar-preview {
        width: 192px;
        height: 192px;
        position: relative;
        border-radius: 100%;
        border: 6px solid #F8F8F8;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
    }

    .avatar-upload .avatar-preview>div {
        width: 100%;
        height: 100%;
        border-radius: 100%;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
    }

    .modal-backdrop {
        z-index: 0 !important;
    }

    .modal-body {
        height: 370px !important;
    }

</style>
        <div class="container-fluid">
            <div class="row">
                <div class="card">
                    <div class="card-header">
                        <strong>Account Setting</strong>
                    </div>
                    <div class="card-body card-block">
                        @if(Session::has('message'))
                        <div class="alert  alert-success alert-dismissible fade show" role="alert">
                            {{ session('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close" onClick="this.parentElement.style.display='none';">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <form action="{{ route('profileupdate', $id) }}" method="post" enctype="multipart/form-data">
                            <div class="row">
                                @csrf
                                <input type="hidden" name="_method" value="PATCH" />
                                <div class="avatar-upload">
                                    <div class="avatar-edit">
                                        <input type='file' id="imageUpload" accept=".png, .jpg, .jpeg" name="mypic" @if(isset($User[0]->avatar)) value="{{ $User[0]->avatar }}" @else value="" @endif/>
                                        <label for="imageUpload"></label>
                                    </div>
                                    <div class="avatar-preview">
                                        <div id="imagePreview" @if(Auth::user()->avatar) style="background-image: url(public/users-avatar/{{ $User[0]->avatar }});" @else style="background-image: url(assets/images/users/1.jpg)" @endif ></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="firstname">Name</label>
                                        <input type="text" class="form-control" required id="firstname" required name="name" value="{{$User[0]->name}}">
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                        <br><br>
                        <h3>To Change Password <a href="#" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Click Here</a></h3>
                    </div>
                </div>
            </div>
        </div>
<!-- The Modal -->
<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Change Your Password</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                @if(Session::has('pmessage'))
                <script>
                    $(function() {
                        $('#myModal').modal('show');
                    });

                </script>
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                    {{ session('pmessage') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close" onClick="this.parentElement.style.display='none';">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                @if ($errors->any())
                <script>
                    $(function() {
                        $('#myModal').modal('show');
                    });
                </script>
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif
                <form action="{{ route('savepasswordset') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <label for="oldpassword" class="col-md-12"><strong>Old Password</strong></label>
                        <div class="col-md-12">
                            <input id="text" name="oldpassword" id="oldpassword" placeholder="Enter old password" class="form-control" type="password">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="newpassword" class="col-md-12"><strong>New Password</strong></label>
                        <div class="col-md-12">
                            <input id="new" name="newpassword" id="newpassword" placeholder="Enter New password" class="form-control" type="password">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="confirmpassword" class="col-md-12"><strong>Confirm New Password</strong></label>
                        <div class="col-md-12">
                            <input id="confirm" name="confirmpassword" id="confirmpassword" placeholder="Confirm Password" class="form-control" type="password">
                        </div>
                    </div>
                    <button class="btn btn-success">Save</button>
                </form>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTAINER-->
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                $('#imagePreview').hide();
                $('#imagePreview').fadeIn(650);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#imageUpload").change(function() {
        readURL(this);
    });

</script>
@endsection
