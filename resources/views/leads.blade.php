@extends('layouts.common')
@section('content')
@php
$url = explode('/', Request::url());
$urlvalue = end($url);
$pageurl=$hashids->decode($urlvalue);
@endphp
<link rel="stylesheet" type="text/css" href="{{asset('assets/extra-libs/multicheck/multicheck.css')}}">
<link href="{{asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
<style>
    .pagination {
        float: right;
    }

</style>
<!-- Page wrapper  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Leads</h4>
            <div class="ml-auto text-right">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Leads</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    @if(Auth::check() && Auth::user()->role == 'admin')
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">File Upload</h5>
            <form action="{{ route('import') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row mb-4 align-items-center">
                    <div class="col-lg-4 col-md-4 pt-1 pr-0">
                        <label for="fileupload"></label>
                        <input type="file" class="form-control" id="fileupload" required name="file">
                    </div>
                    <div class="col-lg-2 col-md-2 pt-4 pl-0">
                        <input type="submit" class="btn btn-success" value="save">
                    </div>
                </div>
            </form>
            <hr>
        </div>
    </div>
    @endif
    <div class="card">
        <div class="card-body">
           @if(isset($pageurl[0]) && $pageurl[0]==10)
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item maintab">
                    <a class="nav-link active" id="allstudents-tab" data-toggle="pill" href="#allstudents" role="tab" aria-controls="allstudents" aria-selected="true">Remainig Leads @if(Auth::check() && Auth::user()->role == 'admin')({{$leadscount}})@endif</a>
                </li>
                @if(Auth::check() && Auth::user()->role == 'admin')
                <li class="nav-item maintab">
                    <a class="nav-link" id="applications-tab" data-toggle="pill" href="#applications" role="tab" aria-controls="applications" aria-selected="false">Assign Leads({{count($assignleads)}})</a>
                </li>
                @endif
            </ul>
            @endif
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="allstudents" role="tabpanel" aria-labelledby="allstudents-tab">
                    <div class="table-responsive">
                        @if (session('success'))
                        <div class="col-sm-12">
                            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                                {{ session('success') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="this.parentElement.style.display='none';">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
<<<<<<< HEAD
                    </div>
                    @endif
                    <h2 class="card-title mt-3 mb-3 pl-2">Leads({{count($leads)}})</h2>
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                @if(Auth::check() && Auth::user()->role == 'admin')
                                <th><input type="checkbox" id="master" class="select-all"></th>
                                @endif
                                <th>#</th>
                                <th>Name</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th>City</th>
                                <th>Project</th>
                                <th>Action</th>
                                @if(isset($pageurl[0]) && $pageurl[0] == 5)
                                    <th>Visit Done</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @if(Auth::check() && (Auth::user()->role == 'admin' || Auth::user()->role == 'user'))
								@foreach($leads as $lead)
							@endif
                        @if(Auth::check() && Auth::user()->role == 'admin')
                        <h2 class="card-title mt-3 mb-3 pl-2">Assign Leads to Users</h2>
                        @endif
                        <form action="{{route('assignleads')}}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            @php
                            $users=\App\User::where('role','user')->get();
                            @endphp
                            @if(Auth::check() && Auth::user()->role == 'admin')
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <select name="" id="user" class="form-control" onchange="return assigning(this.value)">
                                        <option value="">Select User</option>
                                        @foreach($users as $user)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" name="uid" id="uid" value="">
                                </div>
                                <div class="col-md-2">
                                    <input type="submit" name="submit" id="submit" class="btn btn-success">
                                </div>
                            </div>
                            @endif
                            <h2 class="card-title mt-3 mb-3 pl-2">Leads({{count($leads)}})</h2>
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        @if(Auth::check() && Auth::user()->role == 'admin')
                                        <th><input type="checkbox" id="master" class="select-all"></th>
                                        @endif
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Mobile</th>
                                        <th>Email</th>
                                        <th>City</th>
                                        <th>Project</th>
                                        <th>Action</th>
                                        @if(isset($pageurl[0]) && $pageurl[0]==5)
                                        <th>Visit Done</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(Auth::check() && (Auth::user()->role == 'admin' || Auth::user()->role == 'user'))
                                    @foreach($leads as $lead)
                                    @php
                                    $checkrecord=\App\followup::where('lead_id',$lead->lead_id)->count();
                                    $checkmeeting=\App\followup::where('lead_id',$lead->lead_id)->where('status','MeetingSchedule')->first();
                                    @endphp
                                    <tr>
                                        @if(Auth::check() && Auth::user()->role == 'admin')
                                        <td><input type="checkbox" class="sub_chk" value="{{$lead->lead_id}}" name="userlead[]"></td>
                                        @endif
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$lead->name}}</td>
                                        <td>{{$lead->phonenumber}}</td>
                                        <td>{{$lead->email}}</td>
                                        <td>{{$lead->city}}</td>
                                        <td>{{$lead->project}}</td>
                                        <td>
                                            <a href="{{route('viewdetails',$lead->lead_id)}}">View Details</a> <br>
                                            @if($checkrecord > 0)
                                            <a href="javascript:void(0)" onclick="return followuplog({{($lead->lead_id)}});">Last Follow Up</a>
                                            @endif
                                        </td>
                                        @if(isset($pageurl[0]) && $pageurl[0]==5)
                                        <td>
                                            @if(isset($checkmeeting) && ($checkmeeting->status=='MeetingSchedule'))
                                            @if(isset($checkmeeting) && ($checkmeeting->status=='VisitDone'))
                                            <div class="custom-control custom-switch text-center">
                                                <input type="checkbox" class="custom-control-input btn-md" id="switch{{$lead->lead_id}}" checked value="Null" onChange="visitstatus({{ $lead->lead_id}},this.value)">
                                                <label class="custom-control-label" for="switch{{$lead->lead_id}}"></label>
                                            </div>
                                            @else
                                            <div class="custom-control custom-switch text-center">
                                                <input type="checkbox" class="custom-control-input btn-md" id="switch{{$lead->lead_id}}" value="VisitDone" onChange="visitstatus({{ $lead->lead_id}},this.value)">
                                                <label class="custom-control-label" for="switch{{$lead->lead_id}}"></label>
                                            </div>
                                            @endif
                                            @endif
                                        </td>
                                        @endif
                                    </tr>
                                    @endforeach
                                    @endif
<<<<<<< HEAD
                                </td>
                                @if(isset($pageurl[0]) && $pageurl[0] == 5)
                                   <td>
                                    @if(isset($checkmeeting) && ($checkmeeting->status=='MeetingSchedule'))
=======
                                </tbody>
                                <tfoot>
                                    <tr>
                                        @if(Auth::check() && Auth::user()->role == 'admin')
                                        <th><input type="checkbox" id="master"></th>
                                        @endif
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Mobile</th>
                                        <th>Email</th>
                                        <th>City</th>
                                        <th>Project</th>
                                        <th>Action</th>
                                        @if(isset($pageurl[0]) && $pageurl[0]==5)
                                        <th>Visit Done</th>
                                        @endif
                                    </tr>
                                </tfoot>
                            </table>
                        </form>
                        {!! $leads->render() !!}
                    </div>
                </div>
                @if(Auth::check() && Auth::user()->role == 'admin')
                @if(isset($pageurl[0]) && $pageurl[0]==10)
                <div class="tab-pane fade" id="applications" role="tabpanel" aria-labelledby="applications-tab">
                    <div class="table-responsive">
                        @if (session('success'))
                        <div class="col-sm-12">
                            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                                {{ session('success') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="this.parentElement.style.display='none';">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        @endif
                        <h2 class="card-title mt-3 mb-3 pl-2">Assign Leads</h2>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                    <th>City</th>
                                    <th>Project</th>
                                    <th>User</th>
                                    <th>Action</th>
                                    @if(isset($pageurl[0]) && $pageurl[0]==5)
                                    <th>Visit Done</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @if(Auth::check() && (Auth::user()->role == 'admin' || Auth::user()->role == 'user'))
                                @foreach($assignleads as $assignlead)
                                @php
                                $checkrecord=\App\followup::where('lead_id',$assignlead->lead_id)->count();
                                $checkmeeting=\App\followup::where('lead_id',$assignlead->lead_id)->where('status','MeetingSchedule')->first();
                                @endphp
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$assignlead->name}}</td>
                                    <td>{{$assignlead->phonenumber}}</td>
                                    <td>{{$assignlead->email}}</td>
                                    <td>{{$assignlead->city}}</td>
                                    <td>{{$assignlead->project}}</td>
                                    <td>{{$assignlead->username}}</td>
                                    <td>
                                        <a href="{{route('viewdetails',$assignlead->lead_id)}}">View Details</a> <br>
                                        @if($checkrecord > 0)
                                        <a href="javascript:void(0)" onclick="return followuplog({{($assignlead->lead_id)}});">Last Follow Up</a>
                                        @endif
                                    </td>
                                    @if(isset($pageurl[0]) && $pageurl[0]==5)
                                    <td>
                                        @if(isset($checkmeeting) && ($checkmeeting->status=='MeetingSchedule'))
                                        @if(isset($checkmeeting) && ($checkmeeting->status=='VisitDone'))
                                        <div class="custom-control custom-switch text-center">
                                            <input type="checkbox" class="custom-control-input btn-md" id="switch{{$assignlead->lead_id}}" checked value="Null" onChange="visitstatus({{ $assignlead->lead_id}},this.value)">
                                            <label class="custom-control-label" for="switch{{$assignlead->lead_id}}"></label>
                                        </div>
                                        @else
                                        <div class="custom-control custom-switch text-center">
                                            <input type="checkbox" class="custom-control-input btn-md" id="switch{{$assignlead->lead_id}}" value="VisitDone" onChange="visitstatus({{ $assignlead->lead_id}},this.value)">
                                            <label class="custom-control-label" for="switch{{$assignlead->lead_id}}"></label>
                                        </div>
                                        @endif
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                @endforeach
                                @endif
<<<<<<< HEAD
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                        <tfoot>
                            <tr>
                                @if(Auth::check() && Auth::user()->role == 'admin')
                                <th><input type="checkbox" id="master"></th>
                                @endif
                                <th>#</th>
                                <th>Name</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th>City</th>
                                <th>Project</th>
                                <th>Action</th>
                                @if(isset($pageurl[0]) && $pageurl[0] == 5)
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                    <th>City</th>
                                    <th>Project</th>
                                    <th>User</th>
                                    <th>Action</th>
                                    @if(isset($pageurl[0]) && $pageurl[0]==5)
                                    <th>Visit Done</th>
                                    @endif
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                @endif
                @endif
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->

<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- this page js -->
<script src="{{asset('assets/libs/jquery/dist/jquery.min.js')}}" type="application/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.select-all').on('click', function() {
            var checkAll = this.checked;
            $('input[type=checkbox]').each(function() {
                this.checked = checkAll;
            });
        });
    });

    function assigning(uid) {
        $('#uid').val(uid);
    }
    $('#submit').click(function() {
        checked = $("input[type=checkbox]:checked").length;
        var user = $('#user').val();
        if (!checked) {
            alert("You must check at least one checkbox.");
            return false;
        }
        if (user == '') {
            alert("Select User");
            return false;
        }
    });

</script>
@endsection
