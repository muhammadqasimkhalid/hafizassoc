@extends('layouts.common')
@section('content')
<!-- Page wrapper  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Dashboard</h4>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Sales Cards  -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- Column -->
        <div class="col-md-6 col-lg-3 col-xlg-3">
            <a href="{{route('leads',$hashids->encode(1))}}">
                <div class="card card-hover">
                    <div class="box bg-primary text-center">
                        <h1 class="font-light text-white"><i class="fas fa-table"></i></h1>
                        <h3 class="text-white">{{$newlead}}</h3>
                        <h6 class="text-white">New Leads</h6>
                    </div>
                </div>
            </a>
        </div>
        <!-- Column -->
        <div class="col-md-6 col-lg-3 col-xlg-3">
            <a @if($followupleads > 0) href="{{route('leads',$hashids->encode(2))}}" @else href="#" @endif>
                <div class="card card-hover">
                    <div class="box bg-info text-center">
                        <h1 class="font-light text-white"><i class="fas fa-clipboard-check"></i></h1>
                        <h3 class="text-white">{{$followupleads}}</h3>
                        <h6 class="text-white">Follow up</h6>
                    </div>
                </div>
            </a>
        </div>
        <!-- Column -->
        <div class="col-md-6 col-lg-3 col-xlg-3">
            <a href="{{route('leads',$hashids->encode(3))}}">
                <div class="card card-hover">
                    <div class="box bg-success text-center">
                        <h1 class="font-light text-white"><i class="fas fa-thumbs-up"></i></h1>
                        <h3 class="text-white">{{$interestedleads}}</h3>
                        <h6 class="text-white">Intrested</h6>
                    </div>
                </div>
            </a>
        </div>
        <!-- Column -->
        <div class="col-md-6 col-lg-3 col-xlg-3">
            <a href="{{route('leads',$hashids->encode(4))}}">
                <div class="card card-hover">
                    <div class="box bg-danger text-center">
                        <h1 class="font-light text-white"><i class="fas fa-thumbs-down"></i></h1>
                        <h3 class="text-white">{{$notinterestedleads}}</h3>
                        <h6 class="text-white">Not Intrested</h6>
                    </div>
                </div>
            </a>
        </div>
        <!-- Column -->
        <div class="col-md-6 col-lg-3 col-xlg-3">
            <a href="{{route('leads',$hashids->encode(5))}}">
                <div class="card card-hover">
                    <div class="box bg-dark text-center">
                        <h1 class="font-light text-white"><i class="mdi mdi-calendar-clock"></i></h1>
                        <h3 class="text-white">{{$meetingschedule}}</h3>
                        <h6 class="text-white">Meeting Schedule</h6>
                    </div>
                </div>
            </a>
        </div>
        <!-- Column -->
        <div class="col-md-6 col-lg-3 col-xlg-3">
            <a href="#">
                <div class="card card-hover">
                    <div class="box bg-success text-center">
                        <h1 class="font-light text-white"><i class="mdi mdi-call-made"></i></h1>
                        <h3 class="text-white">{{$dialcalls}}</h3>
                        <h6 class="text-white">Dial Call</h6>
                    </div>
                </div>
            </a>
        </div>
        <!-- Column -->
        <div class="col-md-6 col-lg-3 col-xlg-3">
            <a href="#">
                <div class="card card-hover">
                    <div class="box bg-warning text-center">
                        <h1 class="font-light text-white"><i class="mdi mdi-lan-pending"></i></h1>
                        <h3 class="text-white">{{$pendingcalls}}</h3>
                        <h6 class="text-white">Pending Call</h6>
                    </div>
                </div>
            </a>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-md-6 col-lg-3 col-xlg-3">
            <a href="{{route('leads',$hashids->encode(8))}}">
                <div class="card card-hover">
                    <div class="box bg-secondary text-center">
                        <h1 class="font-light text-white"><i class="mdi mdi-call-missed"></i></h1>
                        <h3 class="text-white">{{$noansleads}}</h3>
                        <h6 class="text-white">No Answer</h6>
                    </div>
                </div>
            </a>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-md-6 col-lg-6 col-xlg-3">
            <a href="{{route('leads',$hashids->encode(9))}}">
                <div class="card card-hover">
                    <div class="box bg-success text-center">
                        <h1 class="font-light text-white"><i class="fas fa-handshake"></i></h1>
                        <h3 class="text-white">{{$visitdoneleads}}</h3>
                        <h6 class="text-white">Visit Done</h6>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-6 col-lg-6 col-xlg-3">
            <a href="{{route('leads',$hashids->encode(10))}}">
                <div class="card card-hover">
                    <div class="box bg-warning text-center">
                        <h1 class="font-light text-white"><i class="fas fa-table"></i></h1>
                        <h3 class="text-white">{{$totalleads}}</h3>
                        <h6 class="text-white">Total Leads</h6>
                    </div>
                </div>
            </a>
        </div>
        <!-- Column -->
    </div>
    <!-- ============================================================== -->
    <!-- Charts -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
               <canvas id="oilChart" width="300" height="100"></canvas>
            </div>
        </div>
    </div>
    <!-- End Charts -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Recent comment and chats -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Recent comment and chats -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- End Page wrapper  -->
@endsection
