@extends('layouts.common')
@section('content')
<script src="{{asset('assets/libs/jquery/dist/jquery.min.js')}}" type="application/javascript"></script>
<!-- Page wrapper  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Dashboard</h4>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <button class="btn btn-success btn-md" onclick="return adduser();">
                <h5 class="card-title mb-0 p-1">Add New User</h5>
            </button>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <h5 class="card-title m-b-0">User Table</h5>
        </div>
        @if(Session::has('updatesuccess'))
        <div class="alert  alert-success alert-dismissible fade show" role="alert">
            {{ session('updatesuccess') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close" onClick="this.parentElement.style.display='none';">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
        <table class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Picture</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Role</th>
                    <th scope="col">Action</th>
                    <th scope="col">Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td><img @if($user->avatar) src="{{ asset('public/users-avatar')}}/{{$user->avatar}}" alt="{{Auth::user()->name}}" @else src="{{asset('assets/images/users/1.jpg')}}" alt="User Image" @endif alt="" width="50"></td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->role}}</td>
                    <td>
                        <a href="javascript:void(0)" onclick="return edituser({{($user->id)}});" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i></a>
                        <!--a href="#" class="btn btn-sm btn-danger"><i class="fas fa-trash-alt"></i></a-->
                        <form method="post" style="width: 23%; float: left;" class="delete_form" action="{{route('userdelete',$user->id)}}">
                            {{csrf_field()}}
                            <input type="hidden" name="_method" value="DELETE" />
                            <button class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this user?')" type="submit"><i class="fas fa-trash"></i></button>
                        </form>
                    </td>
                    <td>
                       @if($user->status == 1)
                        <div class="custom-control custom-switch text-center">
                            <input type="checkbox" class="custom-control-input btn-md" id="switch{{$user->id}}" checked value="0" onChange="userStatus({{ $user->id}},this.value)">
                            <label class="custom-control-label" for="switch{{$user->id}}"></label>
                        </div>
                        @else
                        <div class="custom-control custom-switch text-center">
                            <input type="checkbox" class="custom-control-input btn-md" id="switch{{$user->id}}" value="1" onChange="userStatus({{ $user->id}},this.value)">
                            <label class="custom-control-label" for="switch{{$user->id}}"></label>
                        </div>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="adduserrole" tabindex="-1" role="dialog" aria-labelledby="adduserroleLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="adduserroleLabel">Add User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @if(Session::has('success'))
            <script>
                $(function() {
                    $('#adduserrole').modal('show');
                });
            </script>
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close" onClick="this.parentElement.style.display='none';">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            @if ($errors->any())
            <script>
                $(function() {
                    $('#adduserrole').modal('show');
                });
            </script>
            @endif
            <form method="POST" action="{{ route('adduserrole') }}">
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="user_id" id="user_id" value="">
                    <div class="form-group row">
                        <label for="role" class="col-md-4 col-form-label text-md-right">{{ __('Role') }}</label>
                        <div class="col-md-6">
                            <select name="role" id="role" class="form-control">
                                <option value="">Select Role</option>
                                <option value="admin">Admin</option>
                                <option value="user">User</option>
                            </select>
                            @error('role')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="button">
                        {{ __('Register') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- End Page wrapper  -->
@endsection
