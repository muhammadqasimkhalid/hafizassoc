@extends('layouts.common')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('assets/extra-libs/multicheck/multicheck.css')}}">
<link href="{{asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
<style>
    .pagination {
        float: right;
    }

    .form-check {
        text-align: center;
        font-size: 1.5em;
    }

</style>
<!-- Page wrapper  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">View Details</h4>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row justify-content-md-center">
        <div class="col-md-7">
            <div class="card">
                <form class="form-horizontal" action="{{route('followup')}}" method="post">
                    @csrf
                    <input type="hidden" name="lead_id" value="{{$viewlead->id}}">
                    <div class="card-body">
                        <h4 class="card-title">Personal Info</h4>
                        <hr>
                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="fname" value="{{$viewlead->name}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-3 text-right control-label col-form-label">Email</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" id="email" value="{{$viewlead->email}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="phonenumber" class="col-sm-3 text-right control-label col-form-label">Contact Number</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="phonenumber" value="{{$viewlead->phonenumber}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="alternateno" class="col-sm-3 text-right control-label col-form-label">Alternative Number</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="alternateno" value="{{$viewlead->alternatephone}}" name="alternateno">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="city" class="col-sm-3 text-right control-label col-form-label">City</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="city" value="{{$viewlead->city}}" name="city">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="project" class="col-sm-3 text-right control-label col-form-label">Project</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="project" value="{{$viewlead->project}}" name="project">
                            </div>
                        </div>
                        <hr>
                        <h4 class="card-title">Status</h4>
                        <hr>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" value="Answered" name="callstatus" id="passport" onclick="javascript:PassportCheck();" required>Answered
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" value="NoAnswered" name="callstatus" id="passportno" onclick="javascript:PassportCheck();" required>Not Answered
                            </label>
                        </div>
                        <hr>
                        <div id="PassportifYes" style="display:none;">
                            <div class="form-group row">
                                <label for="status" class="col-sm-3 text-right control-label col-form-label">Status</label>
                                <div class="col-sm-9">
                                    <select name="status" id="status" class="form-control">
                                        <option value="">Select Status</option>
                                        <option value="Followup">Follow Up</option>
                                        <option value="Pending">Pending</option>
                                        <option value="Intrested">Intrested</option>
                                        <option value="NotIntrested">Not Intrested</option>
                                        <option value="MeetingSchedule">Meeting Schedule</option>
                                        <option value="VisitDone">Visit Done</option>
                                    </select>
                                </div>
                            </div>

                            <div id="meetingdate" style="display:none">
                                <div class="form-group row">
                                    <label for="date" class="col-sm-3 text-right control-label col-form-label">Date</label>
                                    <div class="col-sm-9">
                                        <input type="date" class="form-control" id="date" name="meetingdate">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="comment" class="col-sm-3 text-right control-label col-form-label">Comment</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" id="comment" name="comment" rows="7" required="required"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12 text-right">
                            <input type="submit" class="btn btn-success" value="Save">
                        </div>
                    </div>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
<script>
    function PassportCheck() {
        if (document.getElementById('passport').checked) {
            document.getElementById('PassportifYes').style.display = 'block';
			$('#status').prop('required',true);
        } else {
			document.getElementById('PassportifYes').style.display = 'none';
			$('#status').prop('required',false);
		}
    }

</script>
@endsection
