@extends('layouts.common')
@section('content')
<!-- Page wrapper  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Report</h4>
            <div class="ml-auto text-right">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Report</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <div class="form-row">				
					<div class="form-group col-md-3">
						@if (Auth::check() && (Auth::user()->role == 'admin'))
							<select name="" id="userreport" class="form-control">
									<option value="">Select User</option>
								@foreach($getuseres as $user)
									<option value="{{$user->name}}">{{$user->name}}</option>
								@endforeach
							</select>
						@endif
					</div>				
                <div class="form-group col-md-8 mb-0 p-0">
                    <div id="reportrange" style="background: #fff; cursor: pointer; padding: 6px 10px; border: 1px solid #ccc;float:right">
                        <i class="fa fa-calendar"></i>&nbsp;
                        <span></span> <i class="fa fa-caret-down"></i>
                    </div>
                </div>
                <div class="form-group col-md-1 float-right mb-0 p-0">
                    <input type="hidden" name="startdate" id="startdate" value="" />
                    <input type="hidden" name="enddate" id="enddate" value="" />
                    <input type="button" name="searchBtn" value="Search" class="btn btn-primary" onclick="return filterbydaterange();" />
                </div>
            </div>
        </div>
    </div>
    <div class="card">
		<canvas id="oilChart" width="300" height="100"></canvas>
        <div class="card-body">
            <h5 class="card-title">Reports</h5>
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="zero_config">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            <th>City</th>
                            <th>Project</th>
                            <th>User</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($assignleads as $assignlead)
                        @php
                        $checkrecord=\App\followup::where('lead_id',$assignlead->lead_id)->count();
                        $checkmeeting=\App\followup::where('lead_id',$assignlead->lead_id)->where('status','MeetingSchedule')->first();
                        @endphp
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$assignlead->name}}</td>
                            <td>{{$assignlead->phonenumber}}</td>
                            <td>{{$assignlead->email}}</td>
                            <td>{{$assignlead->city}}</td>
                            <td>{{$assignlead->project}}</td>
                            <td>{{$assignlead->username}}</td>
                            <td>
                                <a href="{{route('viewdetails',$assignlead->lead_id)}}">View Details</a> <br>
                                @if($checkrecord > 0)
                                <a href="javascript:void(0)" onclick="return followuplog({{($assignlead->lead_id)}});">Last Follow Up</a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            <th>City</th>
                            <th>Project</th>
                            <th>User</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
