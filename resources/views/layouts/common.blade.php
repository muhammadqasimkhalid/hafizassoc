<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/images/favicon.png')}}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts
    <script src="{{ asset('js/app.js') }}"></script-->
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset('assets/libs/flot/css/float-chart.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{asset('assets/dist/css/style.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js'></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js'></script>
    <![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" data-navbarbg="skin5">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header" data-logobg="skin5">
                    <!-- This is for the sidebar toggle which is visible on mobile only -->
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <a class="navbar-brand" href="#">
                        <!-- Logo icon -->
                        <b class="logo-icon p-l-10">
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="{{asset('assets/images/logo-icon.png')}}" alt="homepage" class="light-logo" />

                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span class="logo-text">
                            <!-- dark Logo text -->
                            <img src="{{asset('assets/images/logo-text.png')}}" alt="homepage" class="light-logo" />
                        </span>
                        <!-- Logo icon -->
                        <!-- <b class="logo-icon"> -->
                        <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                        <!-- Dark Logo icon -->
                        <!-- <img src="{{asset('assets/images/logo-text.png')}}" alt="homepage" class="light-logo" /> -->

                        <!-- </b> -->
                        <!--End Logo icon -->
                    </a>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Toggle which is visible on mobile only -->
                    <!-- ============================================================== -->
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-left mr-auto">
                        <li class="nav-item d-none d-md-block"><a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-24"></i></a></li>
                        <li class="nav-item">
                            <h4 class="pt-4 text-white">Welcome: {{Auth::user()->name}}</h4>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-right">
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-bell font-24"></i>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="margin-left: -45px;">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Comment -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img @if(Auth::user()->avatar) src="{{ asset('public/users-avatar')}}/{{Auth::user()->avatar}}" alt="{{Auth::user()->name}}" @else src="{{asset('assets/images/users/1.jpg')}}" alt="User Image" @endif alt="user" class="rounded-circle" width="31"></a>


                            <div class="dropdown-menu dropdown-menu-right user-dd animated">
                                <a class="dropdown-item" href="{{route('profile')}}"><i class="ti-user m-r-5 m-l-5"></i> My Profile</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('logout') }}" onClick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="fa fa-power-off m-r-5 m-l-5"></i> {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        @include('include.sidebar')
        <div class="page-wrapper">
            @yield('content')
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Hafiz Associates. Designed and Developed by <a href="#">Techlincs</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
    </div>
    <!--Follow Up Log Modal-->
    <div class="modal" id="followuplogmodal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">History</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="followuplogs"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{asset('assets/libs/jquery/dist/jquery.min.js')}}" type="application/javascript"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('assets/extra-libs/sparkline/sparkline.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{asset('assets/dist/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{asset('assets/dist/js/sidebarmenu.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{asset('assets/dist/js/custom.min.js')}}"></script>
    <!--This page JavaScript -->
    <!-- <script src="../../dist/js/pages/dashboards/dashboard1.js')}}"></script> -->
    <!-- Charts js Files -->
    <script src="{{asset('assets/libs/flot/excanvas.js')}}"></script>
    <script src="{{asset('assets/libs/flot/jquery.flot.js')}}"></script>
    <script src="{{asset('assets/libs/flot/jquery.flot.pie.js')}}"></script>
    <script src="{{asset('assets/libs/flot/jquery.flot.time.js')}}"></script>
    <script src="{{asset('assets/libs/flot/jquery.flot.stack.js')}}"></script>
    <script src="{{asset('assets/libs/flot/jquery.flot.crosshair.js')}}"></script>
    <script src="{{asset('assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js')}}"></script>
    <script src="{{asset('assets/dist/js/pages/chart/chart-page-init.js')}}"></script>
    <!-- this page js -->
    <script src="{{asset('assets/libs/chart/matrix.interface.js')}}"></script>
    <script src="{{asset('assets/libs/chart/excanvas.min.js')}}"></script>
    <script src="{{asset('assets/libs/chart/jquery.peity.min.js')}}"></script>
    <script src="{{asset('assets/libs/chart/matrix.charts.js')}}"></script>
    <script src="{{asset('assets/libs/chart/jquery.flot.pie.min.js')}}"></script>
    <script src="{{asset('assets/libs/chart/turning-series.js')}}"></script>
    <script src="{{asset('assets/extra-libs/multicheck/datatable-checkbox-init.js')}}"></script>
    <script src="{{asset('assets/extra-libs/multicheck/jquery.multicheck.js')}}"></script>
    <script src="{{asset('assets/extra-libs/DataTables/datatables.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#zero_config').DataTable({
                dom: 'Blfrtip',
                //fixedHeader: true,
                "paging": true,
                "autoWidth": true,
                "scrollX": true,
                "lengthMenu": [
                    [25, 50, 100, 1000, -1],
                    [25, 50, 100, 1000, "All"]
                ],
                buttons: [
                    'excelHtml5',
                    'pdfHtml5'
                ],
                columnDefs: [{
                    width: "8%",
                    orderable: true,
                    targets: [2,3,4,5,6,7],
                },
            ]
            })
        });

    </script>
    <!--Date Range-->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script>
        var xstartdate = moment().subtract(29, 'days');
        var xenddate = moment();

    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            var cb = function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                $('#startdate').val(start.format('YYYY-MM-DD'));
                $('#enddate').val(end.format('YYYY-MM-DD'));
            };
            var optionSet1 = {
                startDate: xstartdate,
                endDate: xenddate,
                minDate: '01/01/2012',
                maxDate: '12/31/2025',
                /*dateLimit: { days: 365 },*/
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'left',
                buttonClasses: ['btn btn-default'],
                applyClass: 'btn-sm btn-primary',
                cancelClass: 'btn-sm',
                format: 'MM/DD/YYYY',
                separator: ' to ',
                locale: {
                    applyLabel: 'Submit',
                    cancelLabel: 'Clear',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            };
            /* var optionSet2 = {
                startDate: moment().subtract(7, 'days'),
                endDate: moment(),
                opens: 'left',
                ranges: {
                   'Today': [moment(), moment()],
                   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                   'This Month': [moment().startOf('month'), moment().endOf('month')],
                   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
              };*/
            //$('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
            $('#reportrange').daterangepicker(optionSet1, cb);
            $('#reportrange').on('show.daterangepicker', function() {
                console.log("show event fired");
            });
            $('#reportrange').on('hide.daterangepicker', function() {
                console.log("hide event fired");
            });
            $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
                console.log("apply event fired, start/end dates are " +
                    picker.startDate.format('MMMM D, YYYY') +
                    " to " +
                    picker.endDate.format('MMMM D, YYYY')
                );
            });
            $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
                console.log("cancel event fired");
            });
        });

    </script>
    <script>
        $('#reportrange span').html(xstartdate.format('MMMM D, YYYY') + ' - ' + xenddate.format('MMMM D, YYYY'));

    </script>
    <script>
        $(".circle_percent").each(function() {
            var $this = $(this),
                $dataV = $this.data("size"),
                $dataDeg = $dataV * 3.6,
                $round = $this.find(".round_per");
            $round.css("transform", "rotate(" + parseInt($dataDeg + 180) + "deg)");
            $this.append('<div class="circle_inbox"><span class="percent_text"></span></div>');
            $this.prop('Counter', 0).animate({
                Counter: $dataV
            }, {
                duration: 2000,
                easing: 'swing',
                step: function(now) {
                    $this.find(".percent_text").text(Math.ceil(now) + "");
                }
            });
            if ($dataV >= 51) {
                $round.css("transform", "rotate(" + 360 + "deg)");
                setTimeout(function() {
                    $this.addClass("percent_more");
                }, 1000);
                setTimeout(function() {
                    $round.css("transform", "rotate(" + parseInt($dataDeg + 180) + "deg)");
                }, 1000);
            }
        });

    </script>
    <!--Follow Up Log-->
    <script>
        $('input:radio[name="callstatus"]').change(
            function() {
                if ($(this).is(':checked') && $(this).val() == 'NoAnswered') {
                    $('#status').val('');
                }
            });
        $('#status').change(function() {
            var status = $('#status').val();
            if (status == 'MeetingSchedule' || status == 'Followup') {
				$('#date').prop('required',true);
				$('#meetingdate').show();
			} else {
				$('#date').prop('required',false);
				$('#meetingdate').hide();
			}
        })
        //Followup Logs
        function followuplog(lead_id) {
            $.ajax({
                url: "http://edusupport.org/followuplog",
                type: 'GET',
                data: {
                    lead_id: lead_id
                },
                success: function(res, code) {
                    $('.followuplogs').html('');
                    var resultSet = res.commentlogs;
                    for (var i = 0; i < resultSet.length; i++) {
                        var user = resultSet[i].name;
                        var created_at = resultSet[i].created_at;
                        var comments = resultSet[i].comment;
                        $('.followuplogs').append('<table class="table table-striped table-bordered"><tr><th style="width:100px">User</th><td>' + user + '</td></tr><tr><th style="width:100px">Date</th><td>' + created_at + '</td></tr><tr><th style="width:100px">Comment</th><td>' + comments + '</td></tr></table>');
                    };
                    $('#followuplogmodal').modal('show');
                },
                error: function(xhr, textStatus, errorThrown) {
                    //alert(errorThrown);
                }
            });
        }

        function adduser() {
            $('#user_id').val('');
            $('#role').val('');
            $('#name').val('');
            $('#email').val('');
            $('#password').val('');
            $('#button').html('Register');
            $('#adduserroleLabel').html('Add User');
            $('#adduserrole').modal('show');
        }
        //User Update
        function edituser(user_id) {
            $.ajax({
                type: "GET",
                url: "http://edusupport.org/fetchinfo",
                data: {
                    user_id: user_id
                },
                success: function(res, code) {
                    var resultSet = res.result;
                    $('#user_id').val('');
                    $('#role').val('');
                    $('#name').val('');
                    $('#email').val('');
                    $('#password').val('');
                    $('#button').html('');
                    $('#adduserroleLabel').html('');
                    for (var i = 0; i < resultSet.length; i++) {
                        var user_id = resultSet[i].id;
                        var role = resultSet[i].role;
                        var name = resultSet[i].name;
                        var email = resultSet[i].email;
                        //var password = resultSet[i].password;
                        $('#user_id').val(user_id);
                        $('#role').val(role);
                        $('#name').val(name);
                        $('#email').val(email);
                        //$('#password').val(password);
                    };
                    $('#adduserroleLabel').html("Update User");
                    $('#button').html("Update");
                    $('#adduserrole').modal('show');
                }
            })
        }
        //Block user
        function userStatus(userid, statusVal) {
            //alert(statusVal);
            $.ajax({
                type: "GET",
                url: "http://edusupport.org/userStatus",
                data: {
                    status: statusVal,
                    userid: userid
                },
                success: function(res, code) {
                    var resultSet = res.result;
                    //$(".toast-body").text(resultSet);
                    //$('.toast').toast('show');
                    //$(".toast").toast({
                    //    delay: 5000
                    //});
                    //setInterval(function(){ window.location.reload(); }, 2000);	
                    alert(resultSet);
                    location.reload();
                }
            })
        }
        //Visit Status
        function visitstatus(leadid, statusVal) {
            //alert(statusVal);
            $.ajax({
                type: "GET",
                url: "http://edusupport.org/visitstatus",
                data: {
                    status: statusVal,
                    leadid: leadid
                },
                success: function(res, code) {
                    var resultSet = res.result;
                    //$(".toast-body").text(resultSet);
                    //$('.toast').toast('show');
                    //$(".toast").toast({
                    //    delay: 5000
                    //});
                    //setInterval(function(){ window.location.reload(); }, 2000);	
                    alert(resultSet);
                    location.reload();
                }
            })
        }

    </script>
	<script>
		var oilCanvas = document.getElementById("oilChart");
		Chart.defaults.global.defaultFontFamily = "Lato";
		Chart.defaults.global.defaultFontSize = 18;
		
		var oilData = {
			labels: [
				"FollowUp Leads",
				"Interested Leads",
				"Not Interested Leads",
				"New Leads",
				"Meeting Schedule",
				"Noans Leads",
				"Visit Done Leads",
				"Dial Calls",
				"Pending Calls",
				"Total Leads"
			],
			datasets: [
				{
					data: [{{isset($followupleads)?$followupleads:""}}, {{isset($interestedleads)?$interestedleads:""}}, {{isset($notinterestedleads)?$notinterestedleads:""}}, {{isset($newlead)?$newlead:""}}, {{isset($meetingschedule)?$meetingschedule:""}}, {{isset($noansleads)?$noansleads:""}}, {{isset($visitdoneleads)?$visitdoneleads:""}}, {{isset($dialcalls)?$dialcalls:""}}, {{isset($pendingcalls)?$pendingcalls:""}}, {{isset($totalleads)?$totalleads:""}}],
					backgroundColor: [
						"#FF6384",
						"#63FF84",
						"#84FF63",
						"#8463FF",
						"#6384FF",
						"#FF6388",
						"#63FF80",
						"#84FF68",
						"#8463FG",
						"#6384FA"
					]
				}]
		};
		
		var pieChart = new Chart(oilCanvas, {
		  type: 'pie',
		  data: oilData
		});
	</script>
	<script>
		function filterbydaterange(){
			var startdate = $('#startdate').val();
			var enddate = $('#enddate').val();
			var userreport = $('#userreport').val();
			if(startdate == '' && enddate == '' && userreport == ''){
				alert("Please select at least one filter.");
				return false;
			}
			window.location.href = "{{route('report')}}"+'?startdate='+startdate+ '&endate=' +enddate+ '&user=' +userreport;
		}
	</script>
</body>

</html>
