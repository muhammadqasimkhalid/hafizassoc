<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/


Auth::routes();
Route::get('logout', 'LoginController@logout');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/dashboard', 'CommonController@dashboard')->name('dashboard');
Route::get('/usermanagement', 'CommonController@usermanagement')->name('usermanagement');
Route::post('/adduserrole', 'CommonController@adduserrole')->name('adduserrole');
Route::post('import', 'CommonController@import')->name('import');
Route::post('assignleads', 'CommonController@assignleads')->name('assignleads');
Route::post('followup', 'CommonController@followup')->name('followup');
Route::get('leads/{id?}', 'CommonController@leads')->name('leads');
Route::get('viewdetails/{id}', 'CommonController@viewdetails')->name('viewdetails');
Route::get('followuplog', 'CommonController@followuplog')->name('followuplog');
Route::get('fetchinfo', 'CommonController@fetchinfo')->name('fetchinfo');
Route::delete('/userdelete/{id}','CommonController@userdelete')->name('userdelete');
Route::get('userStatus', 'CommonController@userStatus')->name('userStatus');
Route::get('visitstatus', 'CommonController@visitstatus')->name('visitstatus');
Route::get('profile','CommonController@profile')->name('profile');
Route::patch('profileupdate/{id}','CommonController@profileupdate')->name('profileupdate');
Route::post('savepasswordset','CommonController@savepasswordset')->name('savepasswordset');
Route::get('report/','CommonController@report')->name('report');
