<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class assignleads extends Model
{
    protected $guarded=[];
    
    public function assignalllead()
    {
        return $this->hasOne(leads::class, 'id','lead_id');
    }
    
}
