<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Collection;
use Redirect;
use \App\User;
use \App\leads;
use \App\assignleads;
use \App\followup;
use \App\followupslog;
use App\Imports\LeadsImport;
use Maatwebsite\Excel\Facades\Excel;
use Response;
use DB;
use Hashids\Hashids;
use File;
class CommonController extends Controller
{
    public function dashboard(Request $request){
        $current_date = date("Y-m-d");
        if(Auth::check() && Auth::user()->role == 'user' && Auth::user()->status == 1){
            $newlead=assignleads::where('status',0)->where('uid',Auth::id())->count();
            $followupleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('assignleads.uid',Auth::id())->where('followups.status','Followup')->where('followups.meetingdate',$current_date)->count();
            //Interested
            $interestedleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('assignleads.uid',Auth::id())->where('followups.status','Intrested')->count();
            $hashids = new Hashids('min_length',16);
            //NotInterested
            $notinterestedleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('assignleads.uid',Auth::id())->where('followups.status','NotIntrested')->count();
            //Meeting Schedule
            $meetingschedule=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('assignleads.uid',Auth::id())->where('followups.status','MeetingSchedule')->where('followups.meetingdate','!=','')->count();
            //No Answer
            $noansleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('assignleads.uid',Auth::id())->where('followups.callstatus','NoAnswered')->count();
            //Dial Calls
            $dialcalls=assignleads::where('status',1)->where('uid',Auth::id())->count();
            //Dial Calls
            $pendingcalls=assignleads::where('status',0)->where('uid',Auth::id())->count();
            //visitdone
            $visitdoneleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('assignleads.uid',Auth::id())->where('followups.status','VisitDone')->count();
            //Total Leads
            $totalleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('assignleads.uid',Auth::id())->count();
            $hashids = new Hashids('min_length',16);
            return view('dashboard',compact('hashids','followupleads','interestedleads','notinterestedleads','newlead','meetingschedule','noansleads','visitdoneleads','dialcalls','pendingcalls','totalleads'));
        }
        else if(Auth::check() && Auth::user()->role == 'admin'){
            $newlead=assignleads::where('status',0)->count();
            $followupleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('followups.status','Followup')->where('followups.meetingdate',$current_date)->count();
            //Interested
            $interestedleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('followups.status','Intrested')->count();
            $hashids = new Hashids('min_length',16);
            //NotInterested
            $notinterestedleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('followups.status','NotIntrested')->count();
            //Meeting Schedule
            $meetingschedule=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('followups.status','MeetingSchedule')->where('followups.meetingdate','!=','')->count();
            //No Answer
            $noansleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('followups.callstatus','NoAnswered')->count();
            //Dial Calls
            $dialcalls=assignleads::where('status',1)->count();
            //Dial Calls
            $pendingcalls=assignleads::where('status',0)->count();
            //visitdone
            $visitdoneleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('followups.status','VisitDone')->count();
            //Total Leads
            $totalleads=leads::all()->count();
            $hashids = new Hashids('min_length',16);
            return view('dashboard',compact('hashids','followupleads','interestedleads','notinterestedleads','newlead','meetingschedule','noansleads','visitdoneleads','dialcalls','pendingcalls','totalleads'));
        }
        else{
			return redirect('login');
		}
    }
    public function usermanagement(Request $request){
        if(Auth::check() && Auth::user()->role == 'admin'){
            $users=User::orderBy('created_at','desc')->get();
            return view('usermanagement',compact('users'));
        }
        else{
			return redirect('login');
		}
    }
    //add user
    public function adduserrole(Request $request)
    {
        if (Auth::check() && Auth::user()->role == 'admin'){
            $checkrecord=User::where('id',$request->get('user_id'))->first();
            if($checkrecord){
                $request->validate([
                    'name' => ['required', 'string', 'max:255'],
                    'email' => ['required', 'string', 'email', 'max:255'],
                    'role' => ['required'],
                ]);
                $checkrecord->name = $request->get('name');
                $checkrecord->email = $request->get('email');
                $checkrecord->role = $request->get('role');
                $checkrecord->password = Hash::make($request->get('password'));
                $checkrecord->save();
                return redirect('usermanagement')->with('updatesuccess','User has been Updated');
            }
            else{
                $request->validate([
                    'name' => ['required', 'string', 'max:255'],
                    'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                    'password' => ['required', 'string', 'min:8', 'confirmed'],
                    'role' => ['required'],
                ]);                                                                 
                $Userrole = new User([
                    'name' => $request->get('name'),
                    'email'=> $request->get('email'),
                    'role'=> $request->get('role'),
                    'password'=> Hash::make($request->get('password'))
                  ]);
                $Userrole->save();
                return redirect('usermanagement')->with('success','User has been added');
            }
        }
        else
        {
            return Redirect::to('login');    
        }
    }
    public function import(Request $request) 
    {
        if (Auth::check() && Auth::user()->role == 'admin'){
            Excel::import(new LeadsImport,request()->file('file'));
            return back();
        }
        else
        {
            return Redirect::to('login');    
        }
    }
    //All Leads
    public function leads(Request $request,$id=0) 
    {		
        if (Auth::check() && Auth::user()->role == 'admin'){
            //$leads=leads::where('lead_status',1)->paginate(4);
            //return view('leads',compact('leads'));
            	$hashids = new Hashids('min_length',16);
                try 
                {
                    if(isset($hashids->decode($id)[0])){
                        $myid = $hashids->decode($id)[0];
						if($myid == 1){
							$leads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project')->where('assignleads.status','=',0)->paginate(25);
						}
						else if($myid == 2){
							$current_date = date("Y-m-d");
							$leads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('followups.status','Followup')->where('followups.meetingdate',$current_date)->orderBy('leads.created_at','desc')->paginate(25);
						}
						else if($myid == 3){
							$leads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('followups.status','Intrested')->orderBy('leads.created_at','desc')->paginate(25);
						}
						else if($myid == 4){
							$leads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('followups.status','NotIntrested')->orderBy('leads.created_at','desc')->paginate(25);
						}
						else if($myid == 5){
							$leads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('followups.status','MeetingSchedule')->where('followups.meetingdate','!=','')->orderBy('leads.created_at','desc')->paginate(25);
						}
						else if($myid == 8){
							$leads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('followups.callstatus','NoAnswered')->orderBy('leads.created_at','desc')->paginate(25);
						}
						else if($myid == 9){
							$leads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('followups.status','VisitDone')->orderBy('leads.created_at','desc')->paginate(25);
						}
						else if($myid == 10){
                            $leads=leads::select('id as lead_id','name','email', 'phonenumber', 'plot', 'createdDate', 'city', 'project', 'plateform', 'lead_status')->where('lead_status',1)->orderBy('created_at','desc')->paginate(25);
                            $assignleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','users.name as username')->where('lead_status',2)->orderBy('assignleads.created_at','desc')->paginate(25);
                            $leadscount=leads::select('id as lead_id','name','email', 'phonenumber', 'plot', 'createdDate', 'city', 'project', 'plateform', 'lead_status')->where('lead_status',1)->count();
						return view('leads',compact('leads','hashids','assignleads','leadscount'));
						}
						return view('leads',compact('leads','hashids'));
                    }
                    else{
						$leads=leads::select('id as lead_id','name','email', 'phonenumber', 'plot', 'createdDate', 'city', 'project', 'plateform', 'lead_status')->where('lead_status',1)->orderBy('created_at','desc')->paginate(25);
						$leadscount=leads::select('id as lead_id','name','email', 'phonenumber', 'plot', 'createdDate', 'city', 'project', 'plateform', 'lead_status')->where('lead_status',1)->count();
						$assignleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','users.name as username')->where('lead_status',2)->orderBy('assignleads.created_at','desc')->paginate(25);
						return view('leads',compact('leads','hashids','assignleads','leadscount'));
                        //abort(404);
                    }
                }catch (Exception $e) {
                    abort(404);
                }
				
        }
        else if (Auth::check() && (Auth::user()->role == 'user' && Auth::user()->status == 1)){
            $hashids = new Hashids('min_length',16);
                try 
                {
                    if(isset($hashids->decode($id)[0])){
                        $myid = $hashids->decode($id)[0];
                        if($myid == 1){
                        $leads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project')->where('assignleads.uid',Auth::id())->where('assignleads.status','=',0)->paginate(25);
                        }
                        else if($myid == 2){
                        $current_date = date("Y-m-d");
                        $leads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('assignleads.uid',Auth::id())->where('followups.status','Followup')->where('followups.meetingdate',$current_date)->orderBy('leads.created_at','desc')->paginate(25);
                        }
                        else if($myid == 3){
                        $leads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('assignleads.uid',Auth::id())->where('followups.status','Intrested')->orderBy('leads.created_at','desc')->paginate(25);
                        }
                        else if($myid == 4){
                        $leads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('assignleads.uid',Auth::id())->where('followups.status','NotIntrested')->orderBy('leads.created_at','desc')->paginate(25);
                        }
                        else if($myid == 5){
                        $leads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('assignleads.uid',Auth::id())->where('followups.status','MeetingSchedule')->where('followups.meetingdate','!=','')->orderBy('leads.created_at','desc')->paginate(25);
                        }
                        else if($myid == 8){
                        $leads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('assignleads.uid',Auth::id())->where('followups.callstatus','NoAnswered')->orderBy('leads.created_at','desc')->paginate(25);
                        }
                        else if($myid == 9){
                        $leads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('assignleads.uid',Auth::id())->where('followups.status','VisitDone')->orderBy('leads.created_at','desc')->paginate(25);
                        }
                        else if($myid == 10){
                        $leads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project')->where('assignleads.uid',Auth::id())->orderBy('leads.created_at','desc')->paginate(25);
                        }
                        return view('leads',compact('leads','hashids'));
                    }
                    else{
                        $leads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project')->where('assignleads.uid',Auth::id())->orderBy('leads.created_at','desc')->paginate(25);
                        return view('leads',compact('leads','hashids'));
                    }
                }catch (Exception $e) {
                    abort(404);
                }
        }
        else
        {
            return Redirect::to('login');    
        }
    }
    //Assign Leads
    public function assignleads(Request $request) 
    {
        if (Auth::check() && Auth::user()->role == 'admin'){
             $hashids = new Hashids('min_length',16);
            foreach($request->input('userlead') as $value){
                $lead_id=str_replace('"','', $value);
                $assignleads = new assignleads([
                    'uid' => $request->get('uid'),
                    'lead_id'=> $lead_id
                ]);
                $assignleads->save();
                $findlead = leads::find($lead_id);
                $findlead->lead_status = 2;
                $findlead->save();
            }
            return Redirect::route('leads', $hashids->encode(10))->with('success','Leads has been Assigned');
        }
        else
        {
            return Redirect::to('login');    
        }
    }
    //View Leads Details
    public function viewdetails($id) 
    {
        if (Auth::check() && ((Auth::user()->role == 'user' && Auth::user()->status == 1) || Auth::user()->role == 'admin')){
            $viewlead=leads::where('id',$id)->first();
            return view('followup',compact('viewlead'));
        }
        else
        {
            return Redirect::to('login');    
        }
    }
    //Save Follow Up
    public function followup(Request $request) 
    {
        if (Auth::check() && ((Auth::user()->role == 'user' && Auth::user()->status == 1) || Auth::user()->role == 'admin')){
            $hashids = new Hashids('min_length',16);
            $checkrecord=followup::where('lead_id',$request->get('lead_id'))->first();
            $checklead=leads::where('id',$request->get('lead_id'))->first();
            if($checklead){
                $checklead->alternatephone=$request->get('alternateno');
                $checklead->city=$request->get('city');
                $checklead->project=$request->get('project');
                $checklead->save();
            }
            if($request->get('callstatus') == 'NoAnswered'){
                $status = Null;
            }
            else{
                $status=$request->get('status');
            }
            if($request->get('callstatus') != ''){
                $callstatus=$request->get('callstatus');
            }
            else{
                $callstatus=Null;
            }
            if($request->get('meetingdate') != ''){
                $meetingdate=$request->get('meetingdate');
            }
            else{
                $meetingdate=Null;
            }
            if($checkrecord){
                $checkrecord->callstatus = $callstatus;
                $checkrecord->status = $status;
                $checkrecord->meetingdate = $meetingdate;
                $checkrecord->comment = $request->get('comment');
                $checkrecord->save();
                $followupslog = new followupslog([
                    'lead_id' => $request->get('lead_id'),
                    'callstatus'=> $request->get('callstatus'),
                    'status'=> $status,
                    'comment'=> $request->get('comment')
                ]);
                $followupslog->save();
                return Redirect::route('leads', $hashids->encode(10))->with('success','Lead has been updated!...');
            }
            else{
                $followup = new followup([
                    'lead_id' => $request->get('lead_id'),
                    'uid' => Auth::id(),
                    'callstatus'=> $request->get('callstatus'),
                    'status'=> $status,
                    'meetingdate'=> $meetingdate,
                    'comment'=> $request->get('comment')
                ]);
                $followup->save();
                $followupslog = new followupslog([
                    'lead_id' => $request->get('lead_id'),
                    'callstatus'=> $request->get('callstatus'),
                    'status'=> $status,
                    'comment'=> $request->get('comment')
                ]);
                $followupslog->save();
                $assignleads=assignleads::where('lead_id',$request->get('lead_id'))->first();
                if($assignleads){
                    $assignleads->status=1;
                    $assignleads->save();
                }
            return Redirect::route('leads', $hashids->encode(10))->with('success','Comment has been saved!...');
            }
        }
        else
        {
            return Redirect::to('login');    
        }
    }
    //Follow up Log
    public function followuplog(Request $request)
    {
        if (Auth::check() && ((Auth::user()->role == 'user' && Auth::user()->status == 1) || Auth::user()->role == 'admin')){
            $lead_id=$request->get('lead_id');
            $followuplog= DB::table('followupslogs')->join('followups','followups.lead_id','followupslogs.lead_id')->join('users','users.id','=','followups.uid')->select('users.name','followupslogs.comment','followupslogs.created_at')->where('followupslogs.lead_id',$lead_id)->get();
            $sen['commentlogs'] = $followuplog->toArray();
            return Response::json( $sen );
        }
        else{
            return Redirect::to('login');
        }
    }
    //Fetch User Info
    public function fetchinfo(Request $request)
    {
        if (Auth::check() && Auth::user()->role == 'admin'){
            $user_id=$request->get('user_id');
            $user=User::where('id',$user_id)->get();
            $sen['result'] = $user->toArray();
            return Response::json( $sen );
        }
        else{
            return Redirect::to('login');
        }
    }
    //key account delete
    public function userdelete($id)
    {
        if (Auth::check() && Auth::user()->role == 'admin') {
            $user = User::find($id);
            $user->delete();
            return Redirect::back()->with('updatesuccess', 'User has been successfully deleted');
        } else {
            return Redirect::to('login');
        }
    } 
    public function userStatus(Request $request)
    {
        if (Auth::check() && (Auth::user()->role == 'user' || Auth::user()->role == 'admin')){
            $status=$request->get('status');
            $userid=$request->get('userid');
            $user=user::where('id',$userid)->first();
            $user->status = $status;
            $user->save();  
            $sen['result'] = "User Status Changed";
            return Response::json( $sen );
        }
        else{
            return Redirect::to('login');
        }
    }
    //Profile
    public function profile()
    {
        if (Auth::check() && (Auth::user()->role == 'user' || Auth::user()->role == 'admin')){
            $id=Auth::id();
            $User=User::where('id',$id)->get();
            return view('Profile.profile', compact('User','id'));
        }
        else{
            return Redirect::to('login');
        }
    }
    public function profileupdate(Request $request, $id)
    {
        if (Auth::check() && (Auth::user()->role == 'user' || Auth::user()->role == 'admin')){ 
            $userloginID = Auth::id();
            $user= User::where('id', $userloginID)->first();
            $profileImage = $user->avatar;
            if ($files = $request->file('mypic')) {
                // Define upload path
                $destinationPath = public_path('/users-avatar/'); // upload path
                // Upload Orginal Image
                File::delete($destinationPath.$profileImage);
                $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
                $files->move($destinationPath, $profileImage);
                $insert['image'] = "$profileImage";
            }
            $User = User::find($id);
            $User->avatar=$profileImage;
            $User->name=$request->get('name');
            $User->save();
        return Redirect::to('profile')->with('message', 'Profile Successfully Updated!!!');
        } else{
            return Redirect::to('login');  
        }
    }
    //Update New Password
	public function savepasswordset(Request $request){
		if (Auth::check() && (Auth::user()->role == 'user' || Auth::user()->role == 'admin')){  
			$userloginID = Auth::id();
			$user=User::find($userloginID);
			$request->validate([
				'oldpassword' => 'required|min:8',
				'newpassword' => 'required|min:8|same:confirmpassword',
				'confirmpassword' => 'required|min:8',
			]);
			if (Hash::check($request->oldpassword, $user->password)) { 	
				$user->password = Hash::make($request->newpassword);	
				$user->save();
				return Redirect::to('profile')->with('pmessage', 'Password Successfully Updated!!!'); 						
			}else{
				return Redirect::to('profile')->with('pmessage', 'Password dose not match!!!');  
			}      
        } else{
            return Redirect::to('login');  
        }
	}
    //visit lead status
    public function visitstatus(Request $request)
    {
        if (Auth::check() && (Auth::user()->role == 'user' || Auth::user()->role == 'admin')){
            $status=$request->get('status');
            $leadid=$request->get('leadid');
            $followups=followup::where('lead_id',$leadid)->first();
            $followups->status = $status;
            $followups->callstatus = Null;
            $followups->meetingdate = Null;
            $followups->save();  
            $sen['result'] = "User Status Changed";
            return Response::json( $sen );
        }
        else{
            return Redirect::to('login');
        }
    }
    //report
    public function report(Request $request)
    {
		$getuseres = user::where('role','user')->get();
		$current_date = date("Y-m-d");	
        if (Auth::check() && (Auth::user()->role == 'admin')){            		
		    $assignleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','users.name as username')->where('lead_status',2);			
			//All Chart Data
			$newlead=assignleads::where('status',0)->count();
            $followupleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('followups.status','Followup');
            //Interested
            $interestedleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('followups.status','Intrested');
            $hashids = new Hashids('min_length',16);
            //NotInterested
            $notinterestedleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('followups.status','NotIntrested');
            //Meeting Schedule
            $meetingschedule=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('followups.status','MeetingSchedule')->where('followups.meetingdate','!=','');
            //No Answer
            $noansleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('followups.callstatus','NoAnswered');
            //Dial Calls
            $dialcalls=assignleads::where('status',1)->count();
            //Dial Calls
            $pendingcalls=assignleads::where('status',0)->count();
            //visitdone
            $visitdoneleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('followups.status','VisitDone');
            //Total Leads
            $totalleads=leads::all()->count();
            $hashids = new Hashids('min_length',16);
			
			if(isset($request->user)){
				$assignleads = $assignleads->where('users.name',$request->user);
				$followupleads = $followupleads->where('users.name',$request->user);
				$interestedleads = $interestedleads->where('users.name',$request->user);
				$notinterestedleads = $notinterestedleads->where('users.name',$request->user);
				$meetingschedule = $meetingschedule->where('users.name',$request->user);
				$noansleads = $noansleads->where('users.name',$request->user);
				$visitdoneleads = $visitdoneleads->where('users.name',$request->user);
			}
			if(isset($request->startdate) && isset($request->endate)){
				$followupleads = $followupleads->where('followups.meetingdate', '>',$request->startdate)->where('followups.meetingdate', '>',$request->endate);
			}else{
				$followupleads = $followupleads->where('followups.meetingdate',$current_date);
			}
			$assignleads= $assignleads->orderBy('assignleads.created_at','desc')->get();
			$followupleads = $followupleads->count();
			$interestedleads = $interestedleads->count();
			$notinterestedleads = $notinterestedleads->count();
			$meetingschedule = $meetingschedule->count();
			$noansleads = $noansleads->count();
			$visitdoneleads = $visitdoneleads->count();
			return view('report',compact('assignleads','followupleads','interestedleads','notinterestedleads','newlead','meetingschedule','noansleads','visitdoneleads','dialcalls','pendingcalls','totalleads','getuseres'));
        }else if(Auth::check() && Auth::user()->role == 'user' && Auth::user()->status == 1){
			
			 $assignleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','users.name as username')->where('lead_status',2)->orderBy('assignleads.created_at','desc')->get();
			
			
			$newlead=assignleads::where('status',0)->where('uid',Auth::id())->count();
            $followupleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('assignleads.uid',Auth::id())->where('followups.status','Followup');
            //Interested
            $interestedleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('assignleads.uid',Auth::id())->where('followups.status','Intrested')->count();
            $hashids = new Hashids('min_length',16);
            //NotInterested
            $notinterestedleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('assignleads.uid',Auth::id())->where('followups.status','NotIntrested')->count();
            //Meeting Schedule
            $meetingschedule=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('assignleads.uid',Auth::id())->where('followups.status','MeetingSchedule')->where('followups.meetingdate','!=','')->count();
            //No Answer
            $noansleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('assignleads.uid',Auth::id())->where('followups.callstatus','NoAnswered')->count();
            //Dial Calls
            $dialcalls=assignleads::where('status',1)->where('uid',Auth::id())->count();
            //Dial Calls
            $pendingcalls=assignleads::where('status',0)->where('uid',Auth::id())->count();
            //visitdone
            $visitdoneleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('followups','assignleads.lead_id','=','followups.lead_id')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('assignleads.uid',Auth::id())->where('followups.status','VisitDone')->count();
            //Total Leads
            $totalleads=DB::table('assignleads')->join('users','users.id','=','assignleads.uid')->join('leads','assignleads.lead_id','=','leads.id')->select('leads.id as lead_id','leads.name','leads.email','leads.phonenumber','leads.city','leads.project','followups.comment','followups.created_at')->where('assignleads.uid',Auth::id())->count();
            $hashids = new Hashids('min_length',16);
			if(isset($request->startdate) && isset($request->endate)){
				$followupleads = $followupleads->where('followups.meetingdate', '>',$request->startdate)->where('followups.meetingdate', '>',$request->endate);
			}else{
				$followupleads = $followupleads->where('followups.meetingdate',$current_date);
			}
			$followupleads = $followupleads->count();
			return view('report',compact('assignleads','followupleads','interestedleads','notinterestedleads','newlead','meetingschedule','noansleads','visitdoneleads','dialcalls','pendingcalls','totalleads','getuseres'));
		}
        else{
            return Redirect::to('login');
        }
    }
}
