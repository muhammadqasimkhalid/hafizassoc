<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Redirect;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::check() && (Auth::user()->role == 'admin' || (Auth::user()->role == 'user' && Auth::user()->status == 1))){
            return redirect('dashboard');
        }
        else if(Auth::check() && Auth::user()->role == 'user' && Auth::user()->status != 1){
            return view('disable');
        }
        else{
			return redirect('login');
		}
        //return view('home');
    }
}
