<?php

namespace App\Imports;
use App\leads;
use Maatwebsite\Excel\Concerns\ToModel;

class LeadsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new leads([
            'createdDate' => date('Y-m-d', strtotime($row[0])),
            'name' => $row[1],
            'phonenumber' => $row[2],
            'email'    => $row[3], 
            'plot' =>$row[4],
            'plateform' =>$row[5],
            'project' =>$row[6],
            'city' =>$row[7]
        ]);
    }
}
